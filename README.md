# Migrate More Extras

Migrate More Extras contains migrate plugins with only very specific use cases, or plugins that conflict or have overlap with core or migrate_plus plugins.

In general, it is preferable to use core migrate or migrate_plus tools where possible.

This module currently contains:

## Migrate Source REST
A simple source plugin to support importing from arbitrary REST data sources. The plugin supports JSON results and allows passing of credentials to a data source over HTTP Basic Auth.

## Migrate Process Entity Lookup
This plugin allows querying an population of an entity reference field during migration. It is most useful when you must link migration-created content to previously created non-migration content, or when performing a stub-style migration with the linking information on the wrong entity or pointing in the wrong direction.
