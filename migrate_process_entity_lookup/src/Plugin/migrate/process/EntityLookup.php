<?php

/**
 * @file
 * Contains \Drupal\migrate_process_entity_lookup\Plugin\migrate\process\EntityLookup.
 */

namespace Drupal\migrate_process_entity_lookup\Plugin\migrate\process;


use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Uses source values to look up existing entities.
 *
 * This plugin allow you to convert source data into entity IDs via entity
 * query. This is useful in scenarios where creating a stub during migration
 * would not work due to the field data being on the wrong end of the source.
 *
 * @MigrateProcessPlugin(
 *   id = "entity_lookup"
 * )
 */
class EntityLookup extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  protected $entityQuery;
  protected $entityType;
  protected $queryField;
  protected $queryOperation;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $entity_query) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityType = array_key_exists('type', $configuration) ? $configuration['type'] : 'node';
    if (array_key_exists('query', $configuration)) {
      $this->queryOperation = array_key_exists('operation', $configuration['query']) ? $configuration['query']['operation'] : '=';
      $this->queryField = array_key_exists('field', $configuration['query']) ? $configuration['query']['field'] : 'title';
    }
    else {
      $this->queryOperation = '=';
      $this->queryField = 'title';
    }
    $this->entityQuery = $entity_query;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity.query'));
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Do not call parent method for this type.
    /* Get the entity query interface, build the query with our saved
     * configuration and the current value, then reset the array numbering and
     * return it.
     */
    $results = $this->entityQuery->get($this->entityType)
                               ->condition($this->queryField, $value, $this->queryOperation)
                               ->execute();
    return array_values($results);
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    // Do not call parent method.
    return TRUE;
  }

}
