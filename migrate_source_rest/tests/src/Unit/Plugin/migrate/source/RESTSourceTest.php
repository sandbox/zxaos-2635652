<?php
/**
 * @file
 * Contains \Drupal\Tests\migrate_source_rest\Unit\Plugin\migrate\source\RESTSourceTest.
 * User: matt
 * Date: 2015-07-30
 * Time: 11:45
 */

namespace Drupal\Tests\migrate_source_rest\Unit\Plugin\migrate\source;

use Drupal\Tests\UnitTestCase;
use Drupal\migrate_source_rest\Plugin\migrate\source\RESTSource;
use GuzzleHttp\Client;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Subscriber\Mock;


/**
 * @coversDefaultClass \Drupal\migrate_source_rest\Plugin\migrate\source\RESTSource
 *
 * @group migrate_source_rest
 */
class RESTSourceTest extends UnitTestCase {

    /**
     * The plugin id.
     *
     * @var string
     */
    protected $pluginId;

    /**
     * The plugin definition.
     *
     * @var array
     */
    protected $pluginDefinition;

    /**
     * The mock migration plugin.
     *
     * @var \Drupal\migrate\Entity\MigrationInterface
     */
    protected $plugin;

    /**
     * The default configuration values used in the tests.
     *
     * @const array
     */
    private $defaultConfiguration = array(
            'base_scheme' => 'http',
            'base_url' => 'example.com',
            'api_version' => 'v1',
            'uri' => 'test',
    );

    /**
     * The active configuration for the test.
     *
     * @var array
     */
    private $configuration;

    /**
     * The client used to perform request.
     * Separated out here to assist with attaching mock data.
     *
     * @var \GuzzleHttp\Client
     *
     */
    private $client;

    /**
     * @inheritDoc
     */
    protected function setUp() {
        parent::setUp();
        $this->pluginId = 'test csv migration';
        $this->pluginDefinition = array();
        $this->plugin = $this->getMock('\Drupal\migrate\Entity\MigrationInterface');

        $this->configuration = $this->defaultConfiguration;

        $json_body = \GuzzleHttp\Stream\Stream::factory(fopen(dirname(__FILE__).'/RESTSourceTestDataArray.json', 'r'));
        $mock = new Mock([
            new Response(200, [
                'Content-Type' => 'application/json',
                'Cache-Control' => 'public, max-age=0',
                'Connection' => 'close'
                ], $json_body)
            ]);

        $this->client = new Client();
        $this->client->getEmitter()->attach($mock);
    }

    public function testJSONIteratorReturnsArrayObject(){
        $this->configuration['decode'] = 'json';
        $source = new RESTSource($this->configuration, $this->pluginId, $this->pluginDefinition, $this->plugin, $this->client);
        $result = $source->initializeIterator();
        $this->assertInstanceOf("ArrayIterator", $result);
    }

    /**
     * @test
     */
    public function testJSONFields(){
        $this->configuration['decode'] = 'json';
        $source = new RESTSource($this->configuration, $this->pluginId, $this->pluginDefinition, $this->plugin, $this->client);
        $fields = $source->fields();
        $expected_array = ['id', 'text', 'nested_list', 'name'];
        $expected_array = array_combine($expected_array, $expected_array);

        $this->assertArrayEquals($expected_array, $fields);
    }

    /**
     * @test
     */
    public function testKeysSupportNumeric() {
        $this->configuration['keys'] = array(
            'foo',
            'bar'
        );

        $expected = array(
            'foo' => array('type' => 'string'),
            'bar' => array('type'=> 'string')
        );

        $source = new RESTSource($this->configuration, $this->pluginId, $this->pluginDefinition, $this->plugin, $this->client);

        $this->assertArrayEquals($expected, $source->getIds());


    }

    /**
     * @test
     */
    public function testKeysSupportNamed() {
        $this->configuration['keys'] = array(
            'foo' => 'integer',
            'bar' => 'float',
            'baz' => array()
        );

        $expected = array(
            'foo' => array('type' => 'integer'),
            'bar' => array('type'=> 'float'),
            'baz' => array('type'=> 'string')
        );

        $source = new RESTSource($this->configuration, $this->pluginId, $this->pluginDefinition, $this->plugin, $this->client);

        $this->assertArrayEquals($expected, $source->getIds());
    }
}