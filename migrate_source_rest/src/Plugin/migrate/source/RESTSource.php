<?php
/**
 * @file
 * Contains \Drupal\migrate_source_rest\Plugin\migrate\source\RESTSource.
 */

namespace Drupal\migrate_source_rest\Plugin\migrate\source;

use Drupal\migrate\Entity\MigrationInterface;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use GuzzleHttp\Client;

/**
 * Class RESTSource.
 *
 * @package Drupal\migrate_source_rest\Plugin\migrate\source
 *
 * Source for generic REST requests
 *
 * @MigrateSource(
 *  id = "rest"
 * )
 */
class RESTSource extends SourcePluginBase {

  /**
   * @var \GuzzleHttp\Client;
   */
  private $client;

  /**
   * @var array
   *
   * Extra headers to passed to guzzle.
   */
  private $extraHeaders = array();

  /**
   * @inheritDoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, Client $client = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    if (is_null($client)) {
      // Build a default guzzle client.
      $full_base_url = "{$this->configuration['base_scheme']}://{$this->configuration['base_url']}";
      if (isset($this->configuration['api_version'])) {
        $full_base_url .= "/{$this->configuration['api_version']}/";
      }
      $this->client = new Client(['base_uri' => $full_base_url]);

      if (isset($this->configuration['extra_headers'])) {
        $this->extraHeaders = $this->configuration['extra_headers'];
      }
    }
    else {
      $this->client = $client;
    }

    // TODO: Verify any arguments needed here.
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {

    $response = $this->client->get($this->configuration['uri'], $this->extraHeaders);

    if ($response->getStatusCode() == 200) {
      switch ($this->configuration['decode']) {
        case 'lines':
          $body = $response->getBody();
          return new \ArrayIterator(explode("\n", str_replace(["\r\n", "\r"], "\n", "\n", $body)));

        case 'json':
          // TODO: Handle object / single responses by wrapping with array.
          return new \ArrayIterator(json_decode($response->getBody()->getContents(), TRUE));

        case 'raw':
          // Fall through.
        default:
          return $response->getBody();
      }
    }

    // TODO: Handle request error gracefully.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = array();
    // If we have numeric keys, assume a list of ids (with string type).
    // Otherwise, trust the config to specify type.
    foreach ($this->configuration['keys'] as $key => $value) {
      // If numeric...
      if (is_int($key)) {
        $ids[$value]['type'] = 'string';
      }
      else {
        if (is_array($value) and count($value) == 0) {
          $ids[$key]['type'] = 'string';
        }
        else {
          $ids[$key]['type'] = $value;
        }
      }
    }
    return $ids;
  }

  public function __toString()
  {
    //Can this be called before initializeIterator?
    //If so, can we get guzzle to resolve the URL anyway?
    //If not, can we get the URL out of guzzle client?
    $description = "{$this->configuration['base_scheme']}://{$this->configuration['base_url']}";
    if (isset($this->configuration['api_version'])) {
      $description .= "/{$this->configuration['api_version']}/";
    }
    $description .= $this->configuration['uri'];
    return $description;
  }

  /**
   * Returns available fields on the source.
   *
   * @return array
   *   Available fields in the source, keys are the field machine names as used
   *   in field mappings, values are descriptions.
   *   This will be derived from the keys of the first result returned
   */
  public function fields()
  {
    $i = $this->getIterator();
    $i = $i[0];
    return array_combine(array_keys($i), array_keys($i));
  }


}
